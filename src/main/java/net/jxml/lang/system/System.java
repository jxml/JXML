package net.jxml.lang.system;

import net.jxml.bean.AbstractVariableClass;
import net.jxml.bean.Class;
import net.jxml.bean.Methodable;
import net.jxml.bean.VariableManager;
import net.jxml.model.CallModel;
import net.jxml.model.ClassModel;
import net.jxml.model.StartModel;

import java.util.Arrays;
import java.util.List;

/**
 * The system class bean for the XDK.
 *
 * @since 1.0-SNAPSHOT
 * @author PizzaCrust
 */
@Class(reference = "system.System")
public final class System extends AbstractVariableClass implements Methodable {

    public System() {
        declare(
                "systemVersion",
                java.lang.System.getProperty("os.version"),
                "StringJXML"
        );
    }

    private final String[] METHODS = new String[] {
      "print",
    };

    @Override
    public boolean isCorrectFormat(CallModel callModel) {
        if (callModel.getMethodName().equals("print")) {
            if (callModel.getParameters().length < 0) {
                return false;
            }
        }
        return true;
    }

    @Override
    public String handleExecution(StartModel startModel, ClassModel classModel, CallModel callModel) {
        if (callModel.getMethodName().equals("print")) {
            String outputString = VariableManager.translateString(startModel, classModel.getImports(), callModel.getOriginalParameters());
            java.lang.System.out.println(outputString);
            return null;
        }
        return null;
    }
    @Override
    public List<String> methods() {
        return Arrays.asList(METHODS);
    }
}
