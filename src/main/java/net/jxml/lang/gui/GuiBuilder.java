package net.jxml.lang.gui;

import net.jxml.bean.Class;
import net.jxml.bean.Methodable;
import net.jxml.model.CallModel;
import net.jxml.model.ClassModel;
import net.jxml.model.StartModel;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * The GUI builder for creating GUIs in the XDK bean.
 *
 * @since 1.0-SNAPSHOT
 * @author PizzaCrust
 */
@Class(reference = "gui.GuiBuilder")
public final class GuiBuilder implements Methodable {
    /**
     * The registered gui objects.
     */
    public static final ArrayList<GuiContainer> GUI_OBJECTS = new ArrayList<>();

    /**
     * The methods inside of this class.
     */
    private final String[] METHODS = new String[] {
        "createInterfaceObject", "title", "show", "hold"
    };

    @Override
    public boolean isCorrectFormat(CallModel callModel) {
        if (callModel.getMethodName().equals("createInterfaceObject")) {
            if (callModel.getParameters().length < 0) {
                return false;
            }
        }
        if (callModel.getMethodName().equals("title")) {
            if (callModel.getParameters().length < 2) {
                return false;
            }
        }
        if (callModel.getMethodName().equals("show")) {
            if (callModel.getParameters().length < 0) {
                return false;
            }
        }
        return true;
    }

    @Override
    public String handleExecution(StartModel startModel, ClassModel classModel, CallModel callModel) {
        if (callModel.getMethodName().equals("createInterfaceObject")) {
            String id = callModel.getParameters()[0];
            try {
                Integer.parseInt(id);
            } catch (NumberFormatException e) {
                return e.getMessage();
            }
            int integerId = Integer.parseInt(id);
            GuiContainer guiContainer = new GuiContainer(integerId);
            GUI_OBJECTS.add(guiContainer);
            return null;
        }
        if (callModel.getMethodName().equals("title")) {
            String id = callModel.getParameters()[0];
            try {
                Integer.parseInt(id);
            } catch (NumberFormatException e) {
                return e.getMessage();
            }
            int integerId = Integer.parseInt(id);
            String title = callModel.getParameters()[1];
            for (GuiContainer guiContainer : GUI_OBJECTS) {
                if (guiContainer.getID() == integerId) {
                    guiContainer.setTitle(title);
                }
            }
            return null;
        }
        if (callModel.getMethodName().equals("show")) {
            String id = callModel.getParameters()[0];
            try {
                Integer.parseInt(id);
            } catch (NumberFormatException e) {
                return e.getMessage();
            }
            int integerId = Integer.parseInt(id);
            for (GuiContainer guiContainer : GUI_OBJECTS) {
                if (guiContainer.getID() == integerId) {
                    if (guiContainer.getTitle() == null) {
                        return "NPE: No title for GuiContainer";
                    }
                    JFrame jFrame = new JFrame(guiContainer.getTitle());
                    jFrame.setVisible(true);
                }
            }
            return null;
        }
        if (callModel.getMethodName().equals("hold")) {
            while (true) {}
        }
        return null;
    }

    @Override
    public List<String> methods() {
        return Arrays.asList(METHODS);
    }
}
