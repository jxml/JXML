package net.jxml.lang.gui;

/**
 * An GUI container for the XDK class {@link GuiBuilder}.
 *
 * @since 1.0-SNAPSHOT
 * @author PizzaCrust
 */
public class GuiContainer {
    /**
     * The id for the container.
     */
    private final int id;

    /**
     * The title of the GUI.
     */
    private String title;

    /**
     * Constructs a new {@link GuiContainer}.
     * @param id the id
     */
    public GuiContainer(int id) {
        this.id = id;
    }

    /**
     * Retrieves the title of the container.
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Retrieves the ID of the container.
     * @return the ID
     */
    public int getID() {
        return id;
    }

    /**
     * Sets the title of the container.
     * @param title the title
     */
    public void setTitle(String title) {
        this.title = title;
    }
}
