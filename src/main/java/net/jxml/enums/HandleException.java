package net.jxml.enums;

/**
 * Represents handlers of exceptions.
 *
 * @since 1.0-SNAPSHOT
 * @author PizzaCrust
 */
public enum HandleException {
    /**
     * Prints the stack trace of the exception.
     */
    PRINT_TRACE,

    /**
     * The handler will not do anything.
     */
    DO_NOTHING,

    /**
     * Prints the stack trace of the exception and exits the application.
     */
    PRINT_TRACE_AND_EXIT,

    /**
     * The handler will just exit the application.
     */
    EXIT,
}
