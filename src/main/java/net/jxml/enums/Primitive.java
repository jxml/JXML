package net.jxml.enums;

/**
 * Represents the XDK's primitives.
 *
 * @since 1.0-SNAPSHOT
 * @author PizzaCrust
 */
public enum Primitive {
    INTEGER("IntegerJXML"),
    STRING("StringJXML")
    ;

    /**
     * The string representation.
     */
    private String representation;

    /**
     * Constructs a new {@link Primitive}.
     * @param representation the representation
     */
    Primitive(String representation) {
        this.representation = representation;
    }

    /**
     * Returns the representation.
     * @return the representation
     */
    @Override
    public String toString() {
        return representation;
    }

    /**
     * Retrieves a enum value of Primitive from a Primitive ID.
     * @param name the name
     */
    public static Primitive of(String name) {
        for (Primitive primitive : Primitive.values()) {
            if (primitive.representation.equals(name)) {
                return primitive;
            }
        }
        return null;
    }
}
