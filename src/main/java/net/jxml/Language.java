package net.jxml;

import net.jxml.bean.Classpath;
import net.jxml.compiler.XMLCompiler;
import net.jxml.compiler.XMLDecompiler;
import net.jxml.model.ClassModel;
import net.jxml.token.Runtime;

import java.io.File;

/**
 * JXML's command line class access.
 *
 * @since 1.0-SNAPSHOT
 * @author PizzaCrust
 */
public class Language
{
    /**
     * The language version.
     */
    public static final String LANG_VERSION = "1.0";

    /**
     * The language's compatible XML specification version.
     */
    public static final String XML_SPECIFICATION_VERSION = "1.0";

    public static void main(String[] args) {
        Classpath.loadClassesInClasspath();
        if (args.length == 0) {
            System.out.println("Please specify arguments for this command line application.");
            System.out.println("Do, ? or or h or help or -h or -help to access the help menu.");
            return;
        }
        if (args.length >= 1) {
            if (args[0].equals("-v") || args[0].equals("-version") || args[0].equals("v") || args[0].equals("version")) {
                System.out.println("JXML Version: \"" + LANG_VERSION + "\"");
                return;
            }
            if (args[0].equals("-compiler") || args[0].equals("-c") || args[0].equals("c") || args[0].equals("compiler")) {
                if (args.length >= 2) {
                    System.out.println("Compiling class...");
                    File sourceFile = new File(args[1]);
                    File classFile = new File(sourceFile.getParentFile(), XMLCompiler.removeExtension(sourceFile.getName()) + ".xclass");
                    XMLCompiler compiler = new XMLCompiler(sourceFile);
                    boolean isCreated = true;
                    try {
                        isCreated = classFile.createNewFile();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (!isCreated) {
                        System.out.println("Java couldn't create the file. Manually create a " + classFile.getName() + " in the directory of the XML class file.");
                        return;
                    }
                    try {
                        compiler.compile();
                    } catch (Exception e) {
                        System.out.println("XML Compiler error!");
                        e.printStackTrace();
                    }
                    System.out.println("XML class has been compiled! (" + classFile.getAbsolutePath() + ")");
                    return;
                }
                System.out.println("Insufficent arguments!");
                System.out.println("Usage: jxml -compiler <classLocation>");
                return;
            }
            if (args[0].equals("-run-class") || args[0].equals("-rc") || args[0].equals("run-class") || args[0].equals("rc")) {
                if (args.length >= 2) {
                    System.out.println("Running class...");
                    File sourceFile = new File(args[1]);
                    if (!sourceFile.exists()) {
                        System.out.println("Class doesn't exist!");
                        return;
                    }
                    if (!sourceFile.getName().endsWith(".xclass")) {
                        System.out.println("Not compiled binary or compatible XML class!");
                        return;
                    }
                    XMLDecompiler classDecompiler = new XMLDecompiler(sourceFile);
                    try {
                        ClassModel classModel = classDecompiler.decompile();
                        Runtime.run(classModel);
                    } catch (Exception e) {
                        System.out.println("Failed to run class!");
                        e.printStackTrace();
                    }
                    System.out.println("Class execution has finished.");
                    return;
                }
                System.out.println("Insufficent arguments!");
                System.out.println("Usage: jxml -run-class <classLocation>");
            }
            if (args[0].equals("?") || args[0].equals("help") || args[0].equals("h") || args[0].equals("-help") || args[0].equals("-h")) {
                String[] HELP_MENU = new String[] {
                        "usage: jxml <option> <option-parameters>",
                        "options:",
                        "-compiler <- compiles a specified xml class source file <- parameters: <classLocation>",
                        "-run-class <- runs a compiled .xclass binary file <- parameters: <classLocation>"
                };
                for (String helpEntry : HELP_MENU) {
                    System.out.println(helpEntry);
                }
            }
        }
    }
}
