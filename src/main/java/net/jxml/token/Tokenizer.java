package net.jxml.token;

import net.jxml.enums.HandleException;
import net.jxml.model.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Parses source input streams into 'model's.
 *
 * @since 1.0-SNAPSHOT
 * @author PizzaCrust
 */
public class Tokenizer {

    /**
     * Tokenizes a input stream to a class model.
     *
     * @param inputStream the input stream
     * @return the class model
     */
    public static ClassModel tokenize(InputStream inputStream) throws Exception {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse(inputStream);
        document.getDocumentElement().normalize();
        if (!document.getDocumentElement().getNodeName().equals("class")) {
            throw new Exception();
        }
        String className = document.getDocumentElement().getAttribute("name");
        Node importsElement = document.getDocumentElement().getElementsByTagName("imports").item(0);
        ArrayList<String> importedClass = new ArrayList<>();
        if (importsElement.getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) importsElement;
            NodeList imports = element.getElementsByTagName("import");
            for (int temp = 0; temp < imports.getLength(); temp++) {
                Node node = imports.item(0);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element nodeElement = (Element) node;
                    String importClass = nodeElement.getTextContent();
                    importedClass.add(importClass);
                }
            }
        }
        ArrayList<ImportModel> importModels = new ArrayList<>();
        for (String iClass : importedClass) {
            importModels.add(new ImportModel(iClass));
        }
        ImportGroup importGroup = new ImportGroup(importModels);
        Node startMethodElement = document.getDocumentElement().getElementsByTagName("start").item(0);
        ArrayList<VariableModel> variableModels = new ArrayList<>();
        ArrayList<CallModel> callModels = new ArrayList<>();
        if (startMethodElement.getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) startMethodElement;
            NodeList variables = element.getElementsByTagName("variable");
            for (int temp = 0; temp < variables.getLength(); temp++) {
                Node node = variables.item(0);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element nodeElement = (Element) node;
                    String name = nodeElement.getElementsByTagName("name").item(0).getTextContent();
                    String type = nodeElement.getElementsByTagName("type").item(0).getTextContent();
                    String content = nodeElement.getElementsByTagName("content").item(0).getTextContent();
                    VariableModel model = new VariableModel(name, type, content);
                    variableModels.add(model);
                }
            }
            NodeList calls = element.getElementsByTagName("call");
            for (int temp = 0; temp < calls.getLength(); temp++) {
                Node node = calls.item(0);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element nodeElement = (Element) node;
                    String methodName = nodeElement.getElementsByTagName("method").item(0).getTextContent();
                    String parameters = nodeElement.getElementsByTagName("parameters").item(0).getTextContent();
                    HandleException handleException = null;
                    if (!(nodeElement.getElementsByTagName("handleException").getLength() >= 0)) {
                        handleException = handleException.valueOf(nodeElement.getElementsByTagName("handleException").item(0).getTextContent());
                    }
                    CallModel callModel = new CallModel(methodName, parameters);
                    callModel.setExceptionHandler(handleException);
                    callModels.add(callModel);
                }
            }
        }
        StartModel startModel = new StartModel(variableModels, callModels);
        ClassModel classModel = new ClassModel(className, importGroup, startModel);
        return classModel;
    }
}
