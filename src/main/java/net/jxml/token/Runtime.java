package net.jxml.token;

import net.jxml.bean.MethodManager;
import net.jxml.model.CallModel;
import net.jxml.model.ClassModel;
import net.jxml.model.StartModel;
import org.reflections.Reflections;

/**
 * Represents the JXML runtime.
 *
 * @since 1.0-SNAPSHOT
 * @author PizzaCrust
 */
public class Runtime {
    private Runtime() {}

    /**
     * Runs a {@link ClassModel}.
     * @param classModel the class model
     */
    public static void run(ClassModel classModel) {
        System.out.println("Running class " + classModel.getName() + "...");
        StartModel startModel = classModel.getStartModel();
        for (CallModel callModel : startModel.getCalls()) {
            MethodManager.runMethod(classModel, callModel);
        }
    }
}
