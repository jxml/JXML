package net.jxml.compiler;

import net.jxml.model.ClassModel;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;

/**
 * Represents the XML decompiler.
 *
 * @since 1.0-SNAPSHOT
 * @author PizzaCrust
 */
public class XMLDecompiler
{
    /**
     * The source file.
     */
    private final File sourceFile;

    /**
     * Constructs a new {@link XMLDecompiler}.
     * @param sourceFile the source file
     */
    public XMLDecompiler(File sourceFile) {
        this.sourceFile = sourceFile;
    }

    /**
     * Retrieves the source file.
     * @return the source file
     */
    public File getSourceFile() {
        return sourceFile;
    }

    /**
     * Decompiles the source file specified.
     * @return the class model
     * @throws Exception
     */
    public ClassModel decompile() throws Exception {
        FileInputStream fileInputStream = new FileInputStream(sourceFile);
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        ClassModel classModel = (ClassModel) objectInputStream.readObject();
        objectInputStream.close();
        fileInputStream.close();
        return classModel;
    }
}
