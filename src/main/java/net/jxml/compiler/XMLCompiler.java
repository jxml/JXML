package net.jxml.compiler;

import net.jxml.model.ClassModel;
import net.jxml.token.Tokenizer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

/**
 * Represents the XML compiler.
 *
 * @since 1.0-SNAPSHOT
 * @author PizzaCrust
 */
public class XMLCompiler
{
    /**
     * The source file.
     */
    private final File sourceFile;

    /**
     * Constructs a new {@link XMLCompiler}.
     * @param sourceFile the source file
     */
    public XMLCompiler(File sourceFile) {
        this.sourceFile = sourceFile;
    }

    /**
     * Retrieves the source file target of the compiler.
     * @return the source file target
     */
    public File getSourceFile() {
        return sourceFile;
    }


    /**
     * Removes a extension from a name.
     * @param s the name
     * @return the removed extension name
     */
    public static String removeExtension(String s) {

        String separator = System.getProperty("file.separator");
        String filename;

        // Remove the path upto the filename.
        int lastSeparatorIndex = s.lastIndexOf(separator);
        if (lastSeparatorIndex == -1) {
            filename = s;
        } else {
            filename = s.substring(lastSeparatorIndex + 1);
        }

        // Remove the extension.
        int extensionIndex = filename.lastIndexOf(".");
        if (extensionIndex == -1)
            return filename;

        return filename.substring(0, extensionIndex);
    }

    /**
     * Compiles the specified source file into the directory of the source file.
     * @throws Exception the exception
     */
    public void compile() throws Exception {
        File classFile = new File(sourceFile.getParentFile(), removeExtension(sourceFile.getName()) + ".xclass");
        ClassModel classModel = Tokenizer.tokenize(new FileInputStream(sourceFile));
        classFile.createNewFile();
        FileOutputStream fileOutputStream = new FileOutputStream(classFile);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(classModel);
        objectOutputStream.close();
        fileOutputStream.close();
    }
}
