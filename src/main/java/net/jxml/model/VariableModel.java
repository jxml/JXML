package net.jxml.model;

import java.io.Serializable;

/**
 * Represents 'variable' model.
 *
 * @since 1.0-SNASPHOT
 * @author PizzaCrust
 */
public class VariableModel
    implements Serializable
{
    /**
     * The name of the variable/
     */
    private String name;

    /**
     * The type reference of a module classinside of the classpath.
     */
    private String primitiveType;

    /**
     * The not parsed and compiled, content of the variable.
     */
    private String content;

    /**
     * Constructs a new {@link VariableModel} from a name, primitive type, content, and a boolean.
     * @param name the name of the variable
     * @param primitiveType the primitive type reference
     * @param content the content of the variable
     */
    public VariableModel(String name, String primitiveType, String content) {
        this.name = name;
        this.primitiveType = primitiveType;
        this.content = content;
    }

    /**
     * Retrieves the name of the variable.
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Retrieves the content of the variable.
     * @return the content
     */
    public String getContent() {
        return content;
    }

    /**
     * Retrieves the primitive type reference.
     * @return the primitive type reference
     */
    public String getPrimitiveType() {
        return primitiveType;
    }
}
