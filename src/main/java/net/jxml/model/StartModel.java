package net.jxml.model;

import java.io.Serializable;
import java.util.List;

/**
 * Represents 'start' model.
 *
 * @since 1.0-SNAPSHOT
 * @author PizzaCrust
 */
public class StartModel
    implements Serializable
{
    /**
     * Variables declared in the start model.
     */
    private List<VariableModel> variables;

    /**
     * Method calls inside of the start model.
     */
    private List<CallModel> calls;

    /**
     * Constructs a new {@link StartModel} using variables and calls.
     * @param variables the variables
     * @param calls the calls
     */
    public StartModel(List<VariableModel> variables, List<CallModel> calls) {
        this.variables = variables;
        this.calls = calls;
    }

    /**
     * Retrieves the start model's calls.
     * @return the start model's calls
     */
    public List<CallModel> getCalls() {
        return calls;
    }

    /**
     * Retrieves the start model's variables.
     * @return the start model's variables
     */
    public List<VariableModel> getVariables() {
        return variables;
    }
}
