package net.jxml.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a group of 'import's.
 *
 * @since 1.0-SNAPSHOT
 * @author PizzaCrust
 */
public class ImportGroup
    implements Serializable
{
    /**
     * The 'import's in the group.
     */
    private List<ImportModel> imports = new ArrayList<ImportModel>();

    /**
     * Constructs a new {@link ImportGroup} from {@link ImportModel}.
     * @param models the models
     */
    public ImportGroup(List<ImportModel> models)
    {
        this.imports = models;
    }

    /**
     * Retrieves the imports in the group.
     * @return the imports
     */
    public List<ImportModel> getImports() {
        return imports;
    }
}
