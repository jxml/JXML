package net.jxml.model;

import java.io.Serializable;

/**
 * The 'import' model.
 *
 * @since 1.0-SNAPSHOT
 * @author PizzaCrust
 */
public class ImportModel
    implements Serializable
{
    /**
     * The class identification ID.
     */
    private String classId;

    /**
     * Constructs a new {@link ImportModel}.
     * @param classId the class id
     */
    public ImportModel(String classId)
    {
        this.classId = classId;
    }

    /**
     * Retrieves the class's ID.
     * @return the ID
     */
    public String classID() {
        return classId;
    }
}
