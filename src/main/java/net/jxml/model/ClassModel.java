package net.jxml.model;

import java.io.Serializable;

/**
 * Represents 'class' model.
 *
 * @since 1.0-SNAPSHOT
 * @author PizzaCrust
 */
public class ClassModel
    implements Serializable
{
    /**
     * The name of the class.
     */
    private String name;

    /**
     * The 'imported' references to the class modules in the classpath. Not required.
     */
    private ImportGroup imports = null;

    /**
     * The start method inside of the class, that will be executed.
     */
    private StartModel startModel;

    /**
     * Constructs a new {@link ClassModel} using a name, imports, and a start method.
     * @param name the name
     * @param importGroup the imports
     * @param startModel the start method
     */
    public ClassModel(String name, ImportGroup importGroup, StartModel startModel) {
        this.name = name;
        this.imports = importGroup;
        this.startModel = startModel;
    }

    /**
     * Retrieves the name of the class.
     * @return the name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Retrieves the imports of the class.
     * @return the imports
     */
    public ImportGroup getImports() {
        return imports;
    }

    /**
     * Retrieves the start method of the class.
     * @return the start method
     */
    public StartModel getStartModel() {
        return startModel;
    }
}
