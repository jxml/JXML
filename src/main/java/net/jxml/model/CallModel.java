package net.jxml.model;

import net.jxml.enums.HandleException;

import java.io.Serializable;

/**
 * Represents 'call' model.
 *
 * @since 1.0-SNAPSHOT
 * @author PizzaCrust
 */
public class CallModel
    implements Serializable
{
    /**
     * The method's name.
     */
    private String method;

    /**
     * The parameters given.
     */
    private String[] parameters;

    /**
     * The original non-splitted parameters.
     */
    private String originalParameters;

    /**
     * Handles an exception if the call throws an exception. Not required.
     */
    private HandleException exceptionHandler = null;

    /**
     * Constructs a {@link CallModel} from a method name and parameters.
     * @param method the method name
     * @param parameters the parameters
     */
    public CallModel(String method, String parameters)
    {
        this.method = method;
        this.parameters = parameters
                .split(",");
        this.originalParameters = parameters;
    }

    /**
     * Retrieves the 'splitted' parameters.
     * @return the splitted parameters
     */
    public String[] getParameters() {
        return parameters;
    }

    /**
     * Retrieves the 'name' of the method.
     * @return the name of the method
     */
    public String getMethodName() {
        return method;
    }

    /**
     * Retrieves the 'exception handler' enum.
     * @return the enum
     */
    public HandleException getExceptionHandler() {
        return exceptionHandler;
    }

    /**
     * Sets the exception handler of the model.
     * @param exceptionHandler the exception handler
     */
    public void setExceptionHandler(HandleException exceptionHandler) {
        this.exceptionHandler = exceptionHandler;
    }

    /**
     * Retrieves the original parameters.
     * @return the original parameters
     */
    public String getOriginalParameters() {
        return originalParameters;
    }
}
