package net.jxml.bean;

import org.reflections.Reflections;

import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

/**
 * Represents the classpath.
 *
 * @since 1.0-SNAPSHOT
 * @author PizzaCrust
 */
public class Classpath {
    /**
     * The classpath directory.
     */
    private static final File CLASSPATH_DIRECTORY = new File(System.getProperty("user.dir"), "classpath");

    /**
     * Loaded classes in the classpath.
     */
    private static final List<String> LOADED_CLASSES = new ArrayList<String>();

    /**
     * Adds a URL to the system class loader.
     * @param urls the urls
     * @throws Exception the exception
     */
    private static void addURLs(URL... urls) throws Exception {
        ClassLoader cl = ClassLoader.getSystemClassLoader();
        if(cl instanceof URLClassLoader) {
            URLClassLoader ul = (URLClassLoader)cl;
            java.lang.Class[] paraTypes = new java.lang.Class[]{URL.class};
            Method method = URLClassLoader.class.getDeclaredMethod("addURL", paraTypes);
            method.setAccessible(true);
            Object[] args = new Object[1];

            for(int i = 0; i < urls.length; ++i) {
                args[0] = urls[i];
                method.invoke(ul, args);
            }
        }
    }

    /**
     * Rertieves all classes in a jar.
     * @param jarName the jar
     * @return the classes
     * @throws Exception the exception
     */
    private static String[] getAllClasses(File jarName) throws Exception {
        JarInputStream input = new JarInputStream(new FileInputStream(jarName));
        JarEntry jarEntry;

        ArrayList<String> classNames = new ArrayList<String>();

        while (true) {
            jarEntry = input.getNextJarEntry();
            if (jarEntry == null) {
                break;
            }
            if (jarEntry.getName().endsWith(".class")) {
                String className = jarEntry.getName().substring(0, jarEntry.getName().lastIndexOf('.'));
                className = className.replace('/', '.');
                classNames.add(className);
            }
        }

        return classNames.toArray(new String[classNames.size()]);
    }

    /**
     * Loads all classes inside of the classpath.
     */
    public static void loadClassesInClasspath() {
        if (!CLASSPATH_DIRECTORY.exists()) {
            CLASSPATH_DIRECTORY.mkdir();
        }
        LOADED_CLASSES.clear();
        File[] classpathJars = CLASSPATH_DIRECTORY.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(".jar");
            }
        });
        ArrayList<String> classes = new ArrayList<>();
        ArrayList<URL> fileURLs = new ArrayList<>();
        for (File file : classpathJars) {
            try {
                fileURLs.add(file.toURI().toURL());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            addURLs(fileURLs.toArray(new URL[fileURLs.size()]));
        } catch (Exception e) {
            e.printStackTrace();
        }
        for (File file : classpathJars) {
            try {
                classes.addAll(Arrays.asList(getAllClasses(file)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        LOADED_CLASSES.addAll(classes);
    }

    /**
     * Retrieves a class from the classpath using a reference ID.
     * @param referenceId the reference id
     * @return the class
     */
    public static java.lang.Class<?> from(String referenceId) {
        for (String name : LOADED_CLASSES) {
            try {
                java.lang.Class<?> aClass = java.lang.Class.forName(name);
                if (aClass.isAnnotationPresent(Class.class)) {
                    Class annotation = aClass.getAnnotation(Class.class);
                    if (annotation.reference().equals(referenceId)) {
                        return aClass;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Reflections reflections = new Reflections("net.jxml");
        Set<java.lang.Class<?>> xdkClasses = reflections.getTypesAnnotatedWith(Class.class);
        for (java.lang.Class xdkClass : xdkClasses) {
            Class annotation = (Class) xdkClass.getAnnotation(Class.class);
            if (annotation.reference().equals(referenceId)) {
                return xdkClass;
            }
        }
        return null;
    }
}
