package net.jxml.bean;

import net.jxml.model.VariableModel;

import java.util.ArrayList;
import java.util.List;

/**
 * A bean class that declares variables.
 *
 * @since 1.0-SNAPSHOT
 * @author PizzaCrust
 */
public abstract class AbstractVariableClass {
    /**
     * The declared variables.
     */
    public final List<VariableModel> declaredVariables = new ArrayList<>();

    /**
     * Declares a (public) variable in the class.
     * @param name the name of the variable
     * @param content the content of the variable
     * @param variableReferenceTypeId the class reference id for the type of the variable
     */
    public void declare(String name, String content, String variableReferenceTypeId) {
        declaredVariables.add(new VariableModel(name, variableReferenceTypeId, content));
    }
}
