package net.jxml.bean;

import net.jxml.model.CallModel;
import net.jxml.model.ClassModel;
import net.jxml.model.StartModel;

import java.util.List;

/**
 * Declares the class annotated as 'Methodable'. Which allows the class to be a handler of the specfified class bean.
 *
 * @since 1.0-SNAPSHOT
 * @author PizzaCrust
 */
public interface Methodable {
    /**
     * Retrieves if the call model is in the correct format.
     * @param callModel the call model
     * @return the bool
     */
    boolean isCorrectFormat(CallModel callModel);

    /**
     * Handles a execution of a call model.
     * @param startModel the start method
     * @param classModel the class that is executing
     * @param callModel the call model
     * @return the stack trace, may be null if none is present or compatible
     */
    String handleExecution(StartModel startModel, ClassModel classModel, CallModel callModel);

    /**
     * Retrieves the list of methods declared in this class.
     * @return the list of methods
     */
    List<String> methods();
}
