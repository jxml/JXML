package net.jxml.bean;

import net.jxml.model.CallModel;
import net.jxml.model.ClassModel;
import net.jxml.model.ImportModel;

/**
 * Handles method processing and execution.
 *
 * @since 1.0-SNAPSHOT
 * @author PizzaCrust
 */
public class MethodManager
{
    /**
     * Runs a method specified with a class model and a call modele.
     * @param classModel the class model
     * @param callModel the call model
     */
    public static void runMethod(ClassModel classModel, CallModel callModel) {
        for (ImportModel importModel : classModel.getImports().getImports()) {
            java.lang.Class<?> reflectClass = Classpath.from(importModel.classID());
            if (!Methodable.class.isAssignableFrom(reflectClass)) {
                return;
            }
            try {
                Methodable methodable = (Methodable) reflectClass.newInstance();
                for (String methodName : methodable.methods()) {
                    if (callModel.getMethodName().equals(methodName)) {
                        String stackTrace = methodable.handleExecution(classModel.getStartModel(), classModel, callModel);
                        if (stackTrace == null) {
                            return;
                        }
                        if (callModel.getExceptionHandler() == null) {
                            return;
                        }
                        switch (callModel.getExceptionHandler()) {
                            case PRINT_TRACE:
                                System.out.println(stackTrace);
                                break;
                            case PRINT_TRACE_AND_EXIT:
                                System.out.print(stackTrace);
                                System.exit(-1);
                                break;
                            case DO_NOTHING:
                                break;
                            case EXIT:
                                System.exit(-1);
                                break;
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
