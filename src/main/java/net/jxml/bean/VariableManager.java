package net.jxml.bean;

import net.jxml.enums.Primitive;
import net.jxml.model.*;

/**
 * Handles variable usage.
 *
 * @since 1.0-SNAPSHOT
 * @author PizzaCrust
 */
public class VariableManager
{
    /**
     * Translates a string from imports and content.
     * @param startModel the start model
     * @param model the model
     * @param content the content
     * @return the translated string
     */
    public static String translateString(StartModel startModel, ImportGroup model, String content) {
        String finalString = content;
            for (ImportModel importModel : model.getImports()) {
                java.lang.Class<?> reflectClass = Classpath.from(importModel.classID());
                if (!AbstractVariableClass.class.isAssignableFrom(reflectClass)) {
                    return content;
                }
                try {
                    AbstractVariableClass abstractVariableClass = (AbstractVariableClass) reflectClass.newInstance();
                    for (VariableModel variableModel : abstractVariableClass.declaredVariables) {
                        finalString = finalString.replace("$" + variableModel.getName() + "$", variableModel.getContent());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        for (VariableModel variableModel : startModel.getVariables()) {
            Primitive primitiveType = Primitive.of(variableModel.getPrimitiveType());
            if (primitiveType == null) {
                System.out.println("JXML -> Primitive type is invalid, returning original string.");
                return finalString;
            }
            if (primitiveType == Primitive.INTEGER) {
                try {
                    Integer.parseInt(variableModel.getContent());
                } catch (NumberFormatException e) {
                    System.out.println("JXML -> Variable type is INTEGER, while content does not match.");
                    return finalString;
                }
            }
            finalString = finalString.replace("$" + variableModel.getName() + "$", variableModel.getContent());
        }
        return finalString;
    }
}
