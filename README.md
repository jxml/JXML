# JXML
JXML is a project to create a DSL for Java and using a familar syntax of XML.

## Features
- A lightweight compiler is integrated into this project.
- A full on runtime is integrated.
- Similar structure to Java, such as classpaths are implemented.
- Great way to make your program extensible using JXML.
- Dependencies can be injected officially by JXML's command line tool.
- **Project is extremely active. New features will be implemented quickly, support will respond quickly.**